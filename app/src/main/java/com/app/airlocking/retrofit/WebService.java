package com.app.airlocking.retrofit;


import com.app.airlocking.entities.CredentialEnt;
import com.app.airlocking.entities.TokenEnt;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WebService {
    @POST("/permanent-token/")
    Call<TokenEnt> getPermanentToken(@Body RequestBody credentialEnt);

    @POST("temporary-token/")
    Call<TokenEnt> getTemporaryToken(@Body RequestBody permtoken);
}