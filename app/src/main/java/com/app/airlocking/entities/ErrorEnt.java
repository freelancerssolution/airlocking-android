package com.app.airlocking.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/27/18.
 */
public class ErrorEnt {
    @SerializedName("error")
    private String error;


    public String getError() {
       return error;
    }
}
