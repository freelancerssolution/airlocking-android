package com.app.airlocking.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 5/22/18.
 */
public class TokenEnt {

    @Expose
    @SerializedName("temptoken")
    private String temptoken;
    @Expose
    @SerializedName("permtoken")
    private String permtoken;
    private String pinCode;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPermtoken() {
        return permtoken;
    }

    public String getTemptoken() {
        return temptoken;
    }


}
