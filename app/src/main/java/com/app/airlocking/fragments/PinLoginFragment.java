package com.app.airlocking.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.app.airlocking.R;
import com.app.airlocking.entities.TokenEnt;
import com.app.airlocking.fragments.abstracts.BaseFragment;
import com.app.airlocking.global.WebServiceConstants;
import com.app.airlocking.helpers.UIHelper;
import com.app.airlocking.ui.views.TitleBar;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created on 5/17/18.
 */
public class PinLoginFragment extends BaseFragment {
    public static final String TAG = "PinLoginFragment";
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    @BindView(R.id.btn_login_user)
    TextView btnLoginUser;
    Unbinder unbinder;
    int attemptRemaining = 5;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            if (pin.equalsIgnoreCase(prefHelper.getToken().getPinCode())) {
                JSONObject jsonObject;

                jsonObject = new JSONObject();
                try {
                    jsonObject.put("permtoken", prefHelper.getToken().getPermtoken());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                serviceHelper.enqueueCall(webService.getTemporaryToken(body), WebServiceConstants.TEMPORARY_TOKEN);
            } else {
                attemptRemaining = attemptRemaining - 1;
                if (attemptRemaining == 0) {
                    UIHelper.showShortToastInCenter(getDockActivity(), "Failed to login. Please Register again!");
                    prefHelper.setLoginStatus(false);
                    getDockActivity().popBackStackTillEntry(0);
                    getDockActivity().replaceDockableFragment(LoginFragment.newInstance(false), "LoginFragment");
                } else {
                    pinLockView.resetPinLockView();
                    UIHelper.showShortToastInCenter(getDockActivity(), "Incorrect Pin Code. " + attemptRemaining + " Attempts Remaining");
                }
            }

        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    public static PinLoginFragment newInstance() {
        Bundle args = new Bundle();

        PinLoginFragment fragment = new PinLoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.TEMPORARY_TOKEN:
                String Url = "https://airlocking.co/token-login/?temptoken=" + ((TokenEnt) result).getTemptoken();
                prefHelper.setLoginStatus(true);
                getDockActivity().popBackStackTillEntry(0);
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(Url), "HomeFragment");
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pinlogin, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pinLockView.setPinLockListener(mPinLockListener);
        pinLockView.attachIndicatorDots(indicatorDots);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_login_user)
    public void onViewClicked() {
        getDockActivity().replaceDockableFragment(LoginFragment.newInstance(true),"LoginFragment");
    }
}