package com.app.airlocking.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.app.airlocking.R;
import com.app.airlocking.entities.TokenEnt;
import com.app.airlocking.fragments.abstracts.BaseFragment;
import com.app.airlocking.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created on 5/17/18.
 */
public class PinRegisterFragment extends BaseFragment {
    public static final String TAG = "PinRegisterFragment";

    Unbinder unbinder;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    TokenEnt tokenEnt;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            tokenEnt.setPinCode(pin);
            getDockActivity().replaceDockableFragment(ConfirmPinRegisterFragment.newInstance(tokenEnt), ConfirmPinRegisterFragment.TAG);
        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    public static PinRegisterFragment newInstance(TokenEnt result) {
        Bundle args = new Bundle();

        PinRegisterFragment fragment = new PinRegisterFragment();
        fragment.setArguments(args);
        fragment.setTokenEnt(result);
        return fragment;
    }

    public void setTokenEnt(TokenEnt tokenEnt) {
        this.tokenEnt = tokenEnt;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pin_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pinLockView.setPinLockListener(mPinLockListener);
        pinLockView.attachIndicatorDots(indicatorDots);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}