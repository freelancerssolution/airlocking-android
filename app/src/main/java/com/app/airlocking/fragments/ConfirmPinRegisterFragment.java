package com.app.airlocking.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.app.airlocking.R;
import com.app.airlocking.entities.TokenEnt;
import com.app.airlocking.fragments.abstracts.BaseFragment;
import com.app.airlocking.global.WebServiceConstants;
import com.app.airlocking.helpers.UIHelper;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created on 5/17/18.
 */
public class ConfirmPinRegisterFragment extends BaseFragment {
    public static final String TAG = "ConfPinRegisterFragment";
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.indicator_dots)
    IndicatorDots indicatorDots;
    @BindView(R.id.pin_lock_view)
    PinLockView pinLockView;
    String pinNumber;
    Unbinder unbinder;
    private TokenEnt tokenEnt;
    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.d(TAG, "Pin complete: " + pin);
            if (pin.equalsIgnoreCase(tokenEnt.getPinCode())) {
                prefHelper.putToken(tokenEnt);
                JSONObject jsonObject;

                jsonObject = new JSONObject();
                try {
                    jsonObject.put("permtoken", tokenEnt.getPermtoken());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                serviceHelper.enqueueCall(webService.getTemporaryToken(body), WebServiceConstants.TEMPORARY_TOKEN);
            } else {
                pinLockView.resetPinLockView();
                UIHelper.showShortToastInCenter(getDockActivity(), "Comfirm Pin does not match.");
            }

        }

        @Override
        public void onEmpty() {
            Log.d(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.d(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };

    public static ConfirmPinRegisterFragment newInstance(TokenEnt pin) {
        Bundle args = new Bundle();

        ConfirmPinRegisterFragment fragment = new ConfirmPinRegisterFragment();
        fragment.setPinNumber(pin);
        fragment.setArguments(args);
        return fragment;
    }

    public void setPinNumber(TokenEnt pinNumber) {
        this.tokenEnt = pinNumber;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.TEMPORARY_TOKEN:
                String Url = "https://airlocking.co/token-login/?temptoken=" + ((TokenEnt) result).getTemptoken();
                prefHelper.setLoginStatus(true);
                getDockActivity().popBackStackTillEntry(0);
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(Url), "HomeFragment");
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_pin, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pinLockView.setPinLockListener(mPinLockListener);
        pinLockView.attachIndicatorDots(indicatorDots);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}