package com.app.airlocking.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.airlocking.R;
import com.app.airlocking.entities.TokenEnt;
import com.app.airlocking.fragments.abstracts.BaseFragment;
import com.app.airlocking.global.AppConstants;
import com.app.airlocking.global.WebServiceConstants;
import com.app.airlocking.ui.views.TitleBar;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;


public class LoginFragment extends BaseFragment implements OnClickListener {


    Unbinder unbinder;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.edt_email)
    TextInputEditText edtEmail;
    @BindView(R.id.edt_password)
    TextInputEditText edtPassword;
    @BindView(R.id.btn_register)
    TextView btnRegister;
    @BindView(R.id.btn_forgot_password)
    TextView btnForgotPassword;
    @BindView(R.id.scrollview_bigdaddy)
    LinearLayout scrollviewBigdaddy;
    Unbinder unbinder1;
    private TextWatcher textUpdateListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!edtEmail.getText().toString().equalsIgnoreCase("")
                    && (Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches())
                    && !edtPassword.getText().toString().equalsIgnoreCase("")
                    && !(edtPassword.getText().toString().length() < 6)) {
                loginButton.setEnabled(true);
                loginButton.setBackgroundColor(getDockActivity().getResources().getColor(R.color.app_blue));
            } else {
                loginButton.setEnabled(false);
                loginButton.setBackgroundColor(getDockActivity().getResources().getColor(R.color.app_blue_disabled));
            }
        }
    };
    boolean isFromPin;

    public void setFromPin(boolean fromPin) {
        isFromPin = fromPin;
    }

    public static LoginFragment newInstance(boolean isFromPin) {
        LoginFragment loginFragment = new LoginFragment();
        loginFragment.setFromPin(isFromPin);
        return loginFragment;
    }

    private void setListeners() {
        loginButton.setOnClickListener(this);
        edtEmail.addTextChangedListener(textUpdateListener);
        edtPassword.addTextChangedListener(textUpdateListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder1 = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        setListeners();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void ResponseSuccess(Object result, String Tag) {
        switch (Tag) {
            case WebServiceConstants.PERMANENT_TOKEN:
                if (isFromPin) {
                    JSONObject jsonObject;

                    jsonObject = new JSONObject();
                    try {
                        jsonObject.put("permtoken", ((TokenEnt) result).getPermtoken());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                    serviceHelper.enqueueCall(webService.getTemporaryToken(body), WebServiceConstants.TEMPORARY_TOKEN);
                } else {
                    getDockActivity().replaceDockableFragment(PinRegisterFragment.newInstance((TokenEnt) result), "HomeFragmnet");
                }

                break;
            case WebServiceConstants.TEMPORARY_TOKEN:
                String Url = "https://airlocking.co/token-login/?temptoken=" + ((TokenEnt) result).getTemptoken();
                prefHelper.setLoginStatus(true);
                getDockActivity().popBackStackTillEntry(0);
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(Url), "HomeFragment");
                break;
        }
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        // TODO Auto-generated method stub
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.loginButton:
                JSONObject jsonObject;

                jsonObject = new JSONObject();
                try {
                    jsonObject.put("username", edtEmail.getText().toString());
                    jsonObject.put("password", edtPassword.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

                serviceHelper.enqueueCall(webService.getPermanentToken(body), WebServiceConstants.PERMANENT_TOKEN);

                // prefHelper.setLoginStatus(true);
                //CredentialEnt credentialEnt = new CredentialEnt(edtEmail.getText().toString(), edtPassword.getText().toString());


                break;
        }
    }

    @OnClick({R.id.btn_register, R.id.btn_forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(AppConstants.REGISTER_URL), "HomeFragment");
                break;
            case R.id.btn_forgot_password:
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(AppConstants.FORGOTPASSWORD_URL), "HomeFragment");
                break;
        }
    }
}
