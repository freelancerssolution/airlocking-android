package com.app.airlocking.helpers;

import android.util.Log;


import com.app.airlocking.activities.DockActivity;
import com.app.airlocking.entities.ErrorEnt;
import com.app.airlocking.entities.ResponseWrapper;
import com.app.airlocking.global.WebServiceConstants;
import com.app.airlocking.interfaces.webServiceResponseLisener;
import com.app.airlocking.retrofit.WebService;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.id.message;

/**
 * Created on 7/17/2017.
 */

public class ServiceHelper<T> {
    private webServiceResponseLisener serviceResponseLisener;
    private DockActivity context;
    private WebService webService;

    public ServiceHelper(webServiceResponseLisener serviceResponseLisener, DockActivity conttext, WebService webService) {
        this.serviceResponseLisener = serviceResponseLisener;
        this.context = conttext;
        this.webService = webService;
    }
    public void enqueueCall(Call<ResponseWrapper<T>> call, final String tag) {
        if (InternetHelper.CheckInternetConectivityandShowToast(context)) {
            context.onLoadingStarted();
            call.enqueue(new Callback<ResponseWrapper<T>>() {
                @Override
                public void onResponse(Call<ResponseWrapper<T>> call, Response<ResponseWrapper<T>> response) {
                    context.onLoadingFinished();
                    if (response.code() == 200) {
                        serviceResponseLisener.ResponseSuccess(response.body(), tag);
                    } else {
                        if (response.errorBody() != null) {
                            try {
                                Gson gson = new Gson();
                                ErrorEnt pojo = gson.fromJson(response.errorBody().string(), ErrorEnt.class);
                                UIHelper.showShortToastInCenter(context, pojo.getError() + "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else
                            UIHelper.showShortToastInCenter(context, response.message() + "");
                    }

                }

                @Override
                public void onFailure(Call<ResponseWrapper<T>> call, Throwable t) {
                    context.onLoadingFinished();
                    t.printStackTrace();
                    UIHelper.showShortToastInCenter(context,"Something Went Wrong. Please Try Again!!");
                    Log.e(ServiceHelper.class.getSimpleName()+" by tag: " + tag, t.toString());
                }
            });
        }
    }

}
