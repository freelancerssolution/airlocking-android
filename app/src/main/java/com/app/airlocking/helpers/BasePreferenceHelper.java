package com.app.airlocking.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.app.airlocking.entities.TokenEnt;
import com.app.airlocking.retrofit.GsonFactory;


public class BasePreferenceHelper extends PreferenceHelper {

    protected static final String KEY_LOGIN_STATUS = "islogin";
    protected static final String Firebase_TOKEN = "Firebasetoken";
    protected static final String NotificationCount = "NotificationCount";
    private static final String KEY_TOKEN = "KEY_TOKEN";
    private static final String FILENAME = "preferences";
    private Context context;


    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }

    public TokenEnt getToken() {
        return GsonFactory.getConfiguredGson().fromJson(
                getStringPreference(context, FILENAME, KEY_TOKEN), TokenEnt.class);
    }

    public void putToken(TokenEnt user) {
        putStringPreference(context, FILENAME, KEY_TOKEN, GsonFactory
                .getConfiguredGson().toJson(user));
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }


    public String getFirebase_TOKEN() {
        return getStringPreference(context, FILENAME, Firebase_TOKEN);
    }

    public void setFirebase_TOKEN(String _token) {
        putStringPreference(context, FILENAME, Firebase_TOKEN, _token);
    }

    public int getNotificationCount() {
        return getIntegerPreference(context, FILENAME, NotificationCount);
    }

    public void setNotificationCount(int count) {
        putIntegerPreference(context, FILENAME, NotificationCount, count);
    }


}
