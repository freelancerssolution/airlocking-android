package com.app.airlocking.global;

public class WebServiceConstants {
    public static final String SERVICE_URL = "https://airlocking.co";
    public static final String SUCCESS_RESPONSE_CODE = "2000";
    public static final String PERMANENT_TOKEN = "PERMANENT_TOKEN";
    public static final String TEMPORARY_TOKEN = "TEMPORARY_TOKEN";
    public static final String CONTENT_TYPE = "application/json";
}
