package com.app.airlocking.global;

public class AppConstants {

    public static final String Device_Type = "android";
    public static final String SOCIAL_MEDIA_TYPE = "facebook";
    //Firebase Constants
    // broadcast receiver intent filters
    public static final String TOPIC_GLOBAL = "global";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String REGISTER_URL = "https://airlocking.co/mobileregister";
    public static final String FORGOTPASSWORD_URL = "https://airlocking.co/my-account/lost-password/";
    public static String twitter = "twitter";
    public static int INTENT_ID = 100;
    // id to handle the notification in the notification tray
    public static int NOTIFICATION_ID = 100;
    public static String DateFormat_DMY2 = "dd-MM-yy";
    public static String DateFormat_HM = "HH:mm";
    public static String DateFormat_YMDHMS = "yyyy-MM-dd HH:mm:ss";

}
